package eu.darkh.maptrivia.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Aleksander_Braula on 13-Jan-15.
 */
public class Location {

    private int id;
    private double lat;
    private double lon;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public LatLng getLatLng() {
        return new LatLng(lat, lon);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "[Location: "
                + "id: " + id
                + ", latlon: (" + lat + "," + lon + ")"
                + ", name: " + name
                + "]";
    }
}
