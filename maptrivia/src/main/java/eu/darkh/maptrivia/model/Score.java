package eu.darkh.maptrivia.model;

import eu.darkh.maptrivia.enums.GameDifficulty;
import eu.darkh.maptrivia.enums.GameLength;

/**
 * Created by Aleksander_Braula on 14-Jan-15.
 */
public class Score {

    private int score;
    private long timestamp;
    private String name;
    private GameLength gameLength;
    private GameDifficulty gameDifficulty;

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public GameLength getGameLength() {
        return gameLength;
    }

    public void setGameLength(GameLength gameLength) {
        this.gameLength = gameLength;
    }

    public GameDifficulty getGameDifficulty() {
        return gameDifficulty;
    }

    public void setGameDifficulty(GameDifficulty gameDifficulty) {
        this.gameDifficulty = gameDifficulty;
    }
}
