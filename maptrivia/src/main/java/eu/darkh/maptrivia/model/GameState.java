package eu.darkh.maptrivia.model;

import java.util.HashSet;

import eu.darkh.maptrivia.enums.GameDifficulty;
import eu.darkh.maptrivia.enums.GameLength;

/**
 * Created by darkh on 2015-01-09.
 */
public class GameState {

    private GameLength gameLength;
    private GameDifficulty gameDifficulty;
    private int score;
    private int round;
    private HashSet<Integer> previousLocations = new HashSet<>();

    public GameLength getGameLength() {
        return gameLength;
    }

    public int getGameLengthRounds() {
        return gameLength.getRounds();
    }

    public void setGameLength(GameLength gameLength) {
        this.gameLength = gameLength;
    }

    public GameDifficulty getGameDifficulty() {
        return gameDifficulty;
    }

    public int getRoundTimeMs() {
        return gameDifficulty.getTimeMsPerRound();
    }

    public void setGameDifficulty(GameDifficulty gameDifficulty) {
        this.gameDifficulty = gameDifficulty;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public HashSet<Integer> getPreviousLocations() {
        return previousLocations;
    }

    public void setPreviousLocations(HashSet<Integer> previousLocations) {
        this.previousLocations = previousLocations;
    }

    public void addLocation(int locationId) {
        previousLocations.add(locationId);
    }

    public int getRemainingRounds() {
        return getGameLengthRounds() - round;
    }

    @Override
    public String toString() {
        return "[Game state: "
                + "time per round: " + gameDifficulty.getTimeMsPerRound()
                + ", total rounds: " + gameLength.getRounds()
                + ", current round: " + round
                + ", score: " + score
                + "]";
    }



}
