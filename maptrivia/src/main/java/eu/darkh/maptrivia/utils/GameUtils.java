package eu.darkh.maptrivia.utils;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.google.maps.android.SphericalUtil;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import eu.darkh.library.utils.NetworkUtils;
import eu.darkh.library.utils.StorageUtils;
import eu.darkh.maptrivia.R;
import eu.darkh.maptrivia.enums.Answer;
import eu.darkh.maptrivia.enums.GameDifficulty;
import eu.darkh.maptrivia.enums.GameLength;
import eu.darkh.maptrivia.enums.SPKey;
import eu.darkh.maptrivia.model.Location;
import eu.darkh.maptrivia.model.GameState;
import eu.darkh.maptrivia.model.Score;

/**
 * Created by Aleksander_Braula on 13-Jan-15.
 */
public class GameUtils {

    private static Gson gson;

    public static void initGameUtils(){
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
    }

    public static List<Location> tryToGetCityList() {
        try {
            return getCityList();
        } catch (Exception e) {
            Log.e("MT", "Error while getting city list", e);
        }
        return new ArrayList<>();
    }

    private static List<Location> getCityList() {

        String json;

        if (NetworkUtils.hasNetworkConnection()) {
            Log.d("MT", "Downloading city list from network");
            String url = StorageUtils.getStringResource(R.string.cities_json_url);
            json = NetworkUtils.tryToDownloadDataFromURL(url);
            StorageUtils.putStringInSharedPreferences(SPKey.CITIES.getValue(), json);
        } else {
            Log.d("MT", "Getting stored city list");
            json = StorageUtils.getStringFromSharedPreferences(SPKey.CITIES.getValue());
        }

        return tryToGetListFromJSON(json);
    }

    private static ArrayList<Location> tryToGetListFromJSON(String json) {
        try {
            return getListFromJSON(json);
        } catch (JsonParseException e) {
            Log.e("MT", "Deserialization problem", e);
        }
        return new ArrayList<>();
    }

    private static ArrayList<Location> getListFromJSON(String json) {
        Type type = new TypeToken<ArrayList<Location>>(){}.getType();
        return gson.fromJson(json, type);
    }

    public static void saveGameState(GameState gameState) {
        saveObject(SPKey.GAME_STATE.getValue(), gameState);
    }

    public static GameState tryToLoadGameState() {
        try {
            return loadGameState();
        } catch (Exception e) {

        }
        return new GameState();
    }

    private static GameState loadGameState() {
        String gameStateJSON = StorageUtils.getStringFromSharedPreferences(SPKey.GAME_STATE.getValue());
        return gson.fromJson(gameStateJSON, GameState.class);
    }

    public static void saveScores(ArrayList<Score> scoreList) {
        saveObject(SPKey.SCORES.getValue(), scoreList);
    }

    private static void saveObject(String key, Object o) {
        String JSON = gson.toJson(o);
        StorageUtils.putStringInSharedPreferences(key, JSON);
    }

    public static ArrayList<Score> tryToLoadScores() {
        try {
           return loadScores();
        } catch (Exception e) {

        }
        return new ArrayList<>();
    }

    private static ArrayList<Score> loadScores() {
        Type type = new TypeToken<ArrayList<Score>>(){}.getType();

        String scoresJSON = StorageUtils.getStringFromSharedPreferences(SPKey.SCORES.getValue());

        ArrayList<Score> scoreList = gson.fromJson(scoresJSON, type);

        if (scoreList != null) return scoreList;
        else return new ArrayList<>();
    }

    public static int checkAnswer(LatLng answerLocation, LatLng targetLocation) {
        int distance = (int)SphericalUtil.computeDistanceBetween(answerLocation, targetLocation);
        return calculatePoints(distance);
    }

    private static int calculatePoints(int distance) {
        if (distance > Answer.RANGE) {
            return 0;
        } else {
            return (int)(((float)(Answer.RANGE - distance) / Answer.RANGE) * 100);
        }
    }

    public static void setGameFlag() {
        StorageUtils.putBooleanInSharedPreferences(SPKey.FLAG.getValue(), true);
    }

    public static boolean isGameFlag() {
        return StorageUtils.getBooleanFromSharedPreferences(SPKey.FLAG.getValue());
    }

    public static void removeGameFlag() {
        StorageUtils.putBooleanInSharedPreferences(SPKey.FLAG.getValue(), false);
    }

    public static void saveLastPlayer(String playerName) {
        StorageUtils.putStringInSharedPreferences(SPKey.PLAYER_NAME.getValue(), playerName);
    }

    public static String loadLastPlayer() {
        return StorageUtils.getStringFromSharedPreferences(SPKey.PLAYER_NAME.getValue());
    }

    public static void saveScoreLengthSpinnerState(GameLength gameLength) {
        saveObject(SPKey.SCORE_LENGTH_SPINNER.getValue(), gameLength);
    }

    public static void saveOptionsLengthSpinnerState(GameLength gameLength) {
        saveObject(SPKey.OPTIONS_LENGTH_SPINNER.getValue(), gameLength);
    }

    public static void saveScoreDifficultySpinnerState(GameDifficulty gameDifficulty) {
        saveObject(SPKey.SCORE_DIFFICULTY_SPINNER.getValue(), gameDifficulty);
    }

    public static void saveOptionsDifficultySpinnerState(GameDifficulty gameDifficulty) {
        saveObject(SPKey.OPTIONS_DIFFICULTY_SPINNER.getValue(), gameDifficulty);
    }

    public static GameLength loadScoreLengthSpinnerState() {
        String json = StorageUtils.getStringFromSharedPreferences(SPKey.SCORE_LENGTH_SPINNER.getValue());
        GameLength gameLength = gson.fromJson(json, GameLength.class);
        return  gameLength != null ? gameLength : GameLength.MEDIUM;
    }

    public static GameLength loadOptionsLengthSpinnerState() {
        String json = StorageUtils.getStringFromSharedPreferences(SPKey.OPTIONS_LENGTH_SPINNER.getValue());
        GameLength gameLength = gson.fromJson(json, GameLength.class);
        return  gameLength != null ? gameLength : GameLength.MEDIUM;
    }

    public static GameDifficulty loadScoreDifficultySpinnerState() {
        String json = StorageUtils.getStringFromSharedPreferences(SPKey.SCORE_DIFFICULTY_SPINNER.getValue());
        GameDifficulty gameDifficulty = gson.fromJson(json, GameDifficulty.class);
        return  gameDifficulty != null ? gameDifficulty : GameDifficulty.NORMAL;
    }

    public static GameDifficulty loadOptionsDifficultySpinnerState() {
        String json = StorageUtils.getStringFromSharedPreferences(SPKey.OPTIONS_DIFFICULTY_SPINNER.getValue());
        GameDifficulty gameDifficulty = gson.fromJson(json, GameDifficulty.class);
        return  gameDifficulty != null ? gameDifficulty : GameDifficulty.NORMAL;
    }
}
