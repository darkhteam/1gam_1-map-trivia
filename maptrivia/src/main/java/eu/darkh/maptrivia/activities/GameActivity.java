package eu.darkh.maptrivia.activities;

import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.common.collect.RangeMap;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.TreeSet;

import eu.darkh.library.utils.UiUtils;
import eu.darkh.maptrivia.R;
import eu.darkh.maptrivia.enums.Answer;
import eu.darkh.maptrivia.model.Location;
import eu.darkh.maptrivia.model.GameState;
import eu.darkh.maptrivia.model.Score;
import eu.darkh.maptrivia.utils.GameUtils;

/**
 * Created by Aleksander_Braula on 08-Jan-15.
 */
public class GameActivity extends ActionBarActivity {

    private final static long SECONDS_5 = 5500;
    private final static long SECOND_TICK = 1000;

    private GameState gameState;

    private TextView scoreText;
    private TextView roundText;
    private TextView targetLocationText;
    private ProgressBar timeBar;

    private LinearLayout countdownOverlay;
    private TextView countdownText;

    private CountDownTimer roundTimer;
    private CountDownTimer countdownTimer;

    int counter = 5;

    private GoogleMap googleMap;
    private boolean isAnsweringEnabled = false;

    private DialogInterface.OnClickListener onClickListener;

    private Location targetLocation;

    private Animation fadeout;

    private List<Location> fullLocationList;
    private TreeSet<Integer> preparedLocationIdSet = new TreeSet<>(new Comparator<Integer>() {
        @Override
        public int compare(Integer lhs, Integer rhs) {
            return 1;
        }
    });

    private LoaderTask locationLoaderTask;

    private RangeMap<Integer, Answer> responseMap = Answer.configureResponseMap();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        loadGameState();
        setTitle(gameState.getGameDifficulty().toString() + " Game");
        Log.d("MT", gameState.toString());

        configureUi();
    }

    private void saveGameState() {
        GameUtils.saveGameState(gameState);
    }

    private void loadGameState() {
        gameState = GameUtils.tryToLoadGameState();
    }

    private void configureUi() {
        scoreText = (TextView) findViewById(R.id.score_text);
        updateScoreText();
        roundText = (TextView) findViewById(R.id.round_text);
        updateRoundText();

        targetLocationText = (TextView) findViewById(R.id.target_location_text);

        timeBar = (ProgressBar) findViewById(R.id.time_bar);

        countdownOverlay = (LinearLayout) findViewById(R.id.countdown_overlay);
        countdownText = (TextView) findViewById(R.id.countdown_text);

        configureRoundAlertListener();
        configureFadeAnimation();

        beginStartingGame();

        tryToInitGoogleMap();
    }



    private void configureRoundAlertListener() {
        onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startRound();
                dialog.dismiss();
            }
        };
    }

    private void configureFadeAnimation() {

        fadeout = new AlphaAnimation(1.0f, 0.0f);
        fadeout.setDuration(800);
        fadeout.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                countdownText.setAlpha(0.0f);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }
    private void tryToInitGoogleMap() {
        try {
            initGoogleMap();
        } catch (NullPointerException e) {
            Log.e("MT", "Map error: " + e.getMessage(), e);
            UiUtils.showToastWithText("Map problem");
        }
    }

    private void initGoogleMap() {
        FragmentManager fragmentManager = getFragmentManager();
        MapFragment mapFragment = (MapFragment) fragmentManager.findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap gMap) {
                googleMap = gMap;
                configureGoogleMap();
                startGame();
            }
        });
    }

    private void startGame() {
        disableMapClick();
        if (shouldGameFinish()) {
            finishGame();
        } else {
            startCountdownTimer();
            startLocationLoader();
        }
    }

    private boolean shouldGameFinish() {
        return gameState.getRound() >= gameState.getGameLengthRounds();
    }

    private void configureGoogleMap() {
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (isAnsweringEnabled) {
                    answer(latLng);
                }
            }
        });
    }

    private void startCountdownTimer() {
        countdownTimer.start();
    }

    private void beginStartingGame() {
        configureCountdownTimer();
        showCountdownOverlay();
    }

    @Override
    public void onResume() {
        super.onResume();
        UiUtils.setContext(this);
    }

    private void addMarkerAndFocusCamera(LatLng latLng, int iconId) {
        addMarker(latLng, iconId);
        focusCameraOnLocationAndZoom(latLng, 4);
    }

    private void addMarkerAndFocusCamera(LatLng latLng, int iconId, GoogleMap.CancelableCallback cancelableCallback) {
        addMarker(latLng, iconId);
        focusCameraOnLocationAndZoom(latLng, 4, cancelableCallback);
    }

    private void addMarker(LatLng latLng, int iconId) {
        MarkerOptions markerOptions = new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(iconId));

        googleMap.addMarker(markerOptions);
    }

    private void focusCameraOnLocationAndZoom(LatLng latLng, int zoom) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    private void focusCameraOnLocationAndZoom(LatLng latLng, int zoom, GoogleMap.CancelableCallback cancelableCallback) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom), 1000, cancelableCallback);
    }

    private void addSearchedCircles(LatLng searchedLocation) {
        addCircle(Answer.PERFECT.getCircleOptions(searchedLocation));
        addCircle(Answer.VERY_GOOD.getCircleOptions(searchedLocation));
        addCircle(Answer.GOOD.getCircleOptions(searchedLocation));
        addCircle(Answer.OK.getCircleOptions(searchedLocation));
    }

    private void addCircle(CircleOptions circleOptions) {
        googleMap.addCircle(circleOptions);
    }

    private void clearAllMarkers() {
        googleMap.clear();
    }

    private void updateScoreText(){
        scoreText.setText(gameState.getScore() + "");
    }

    private void updateRoundText(){
        roundText.setText(gameState.getRound() + "");
    }

    private void updateTargetLocationText() {
        targetLocationText.setText(targetLocation.getName());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        tryToCancelTimer(roundTimer);
        tryToCancelTimer(countdownTimer);
        saveGameState();
    }

    private void tryToCancelTimer(CountDownTimer timer) {
        try {
            timer.cancel();
        } catch (Exception e) {

        }
    }

    private void startLocationLoader() {
        cancelLoaderIfRunning();
        locationLoaderTask = new LoaderTask();
        locationLoaderTask.execute();
    }

    private void cancelLoaderIfRunning() {
        if (locationLoaderTask != null && locationLoaderTask.getStatus() == AsyncTask.Status.RUNNING) {
            locationLoaderTask.cancel(true);
        }
    }

    private void continueStartingGame() {
        GameUtils.setGameFlag();
        configureRoundTimer();
        updateRoundText();
        updateScoreText();
        startRound();
    }

    private void prepareCitySet() {
        Random r = new Random(Calendar.getInstance().getTimeInMillis());
        int cityCount = fullLocationList.size();
        int i = 0;
        while (preparedLocationIdSet.size() < (gameState.getRemainingRounds() * 2) && i < cityCount) {
            int id = r.nextInt(cityCount);
            if (!gameState.getPreviousLocations().contains(id) && !preparedLocationIdSet.contains(id)) {
                preparedLocationIdSet.add(id);
            }
            i++;
        }
        Log.d("MT", "Prepared city set: " + preparedLocationIdSet.toString());
    }

    private void finishGame() {
        GameUtils.removeGameFlag();
        showScoreDialog();
    }

    private void goBack() {
        finish();
    }

    private void showScoreDialog() {
        String title = "Congratulation!";
        String body = "You scored " + gameState.getScore() + " points.\n Provide your name for high score:";

        final EditText input = new EditText(UiUtils.getContext());
        String playerName = GameUtils.loadLastPlayer();
        input.setText(playerName != null ? playerName : "Player");
        input.setHint("Player");

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String playerName = input.getText().toString().trim();
                saveScoreAndPlayer(playerName.isEmpty() ? "Player" : playerName);
                dialog.dismiss();
                goBack();
            }
        };

        UiUtils.showAlertDialogWithView(title, body, input, listener);
    }

    private void saveScoreAndPlayer(String playerName) {
        Score score = new Score();
        score.setScore(gameState.getScore());
        score.setTimestamp(DateTime.now().getMillis());
        score.setName(playerName);
        score.setGameLength(gameState.getGameLength());
        score.setGameDifficulty(gameState.getGameDifficulty());

        ArrayList<Score> scoreList = GameUtils.tryToLoadScores();
        scoreList.add(score);

        GameUtils.saveScores(scoreList);
        GameUtils.saveLastPlayer(playerName);
    }

    private void updateScore(int roundScore) {
        int newScore = gameState.getScore() + roundScore;
        gameState.setScore(newScore);
        updateScoreText();
    }

    private void updateRound() {
        int round = gameState.getRound() + 1;
        gameState.setRound(round);
        updateRoundText();
    }

    private void configureRoundTimer() {
        final int timeMsPerRound = gameState.getRoundTimeMs();
        roundTimer = new CountDownTimer(timeMsPerRound, (SECOND_TICK / 10)) {
            @Override
            public void onTick(long millisUntilFinished) {
                int progress = (int)(((float)millisUntilFinished / timeMsPerRound) * 100);
                timeBar.setProgress(progress);
            }

            @Override
            public void onFinish() {
                endRound(-1);
            }
        };
    }

    private void configureCountdownTimer() {
        countdownTimer = new CountDownTimer(SECONDS_5, SECOND_TICK) {
            @Override
            public void onTick(long millisUntilFinished) {
                updateCountdown();
            }

            @Override
            public void onFinish() {
                updateCountdown();
            }
        };
    }

    private void updateCountdown() {
        updateCountdownText();
        counter--;
    }

    private void updateCountdownText() {

        if (counter == 0) {
            Animation out = new AlphaAnimation(0.7f, 0.0f);
            out.setDuration(800);
            out.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    hideCountdownOverlay();
                    continueStartingGame();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            countdownText.setText(R.string.game_go);
            countdownText.setAlpha(1.0f);
            countdownOverlay.startAnimation(out);
        } else {
            countdownText.setText(counter + "");
            countdownText.setAlpha(1.0f);
            countdownText.startAnimation(fadeout);
        }
    }

    private void endRound(final int points) {
        timeBar.setProgress(0);
        disableMapClick();
        LatLng targetLocation = this.targetLocation.getLatLng();
        addSearchedCircles(targetLocation);
        addMarkerAndFocusCamera(targetLocation, R.drawable.searched_icon, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                finishRound(points);
            }

            @Override
            public void onCancel() {

            }
        });

    }

    private void finishRound(int points) {
        Log.d("MT", "End round " + gameState.getRound() + " of " + gameState.getGameLengthRounds());
        if (shouldGameFinish()) {
            finishGame();
        } else {
            showEndRoundAlert(points);
        }
    }

    private void showEndRoundAlert(int points) {
        String title = getString(R.string.round_finished_text);
        String body = responseMap.get(points).getPopupText() + (points < 0 ? "no" : + points) + " points.";

        UiUtils.showAlertDialog(title, body, onClickListener);
    }

    private void startRound() {
        saveGameState();
        updateRound();
        resetMap();
        Location location = tryToDrawLocation();
        setTargetLocation(location);
        Log.d("MT", "Starting round " + gameState.getRound() + ", target: " + location.getName());
        enableMapClick();
        timeBar.setProgress(100);
        roundTimer.start();
    }

    private void resetMap() {
        clearAllMarkers();
        focusCameraOnLocationAndZoom(new LatLng(0.0, 0.0), 1);
    }

    private Location tryToDrawLocation() {
        try {
            return drawLocation();
        } catch (NullPointerException e) {

        }
        return prepareDummyLocation();
    }

    private Location drawLocation() {
        int id = preparedLocationIdSet.pollFirst();
        return fullLocationList.get(id);
    }

    private Location prepareDummyLocation() {
        Location location = new Location();
        location.setId(0);
        location.setLat(0.0);
        location.setLon(0.0);
        location.setName("Dummy location");

        return location;
    }

    private void setTargetLocation(Location location) {
        targetLocation = location;
        gameState.addLocation(location.getId());
        updateTargetLocationText();
    }

    private void enableMapClick() {
        isAnsweringEnabled = true;
        googleMap.getUiSettings().setAllGesturesEnabled(true);
    }

    private void disableMapClick() {
        isAnsweringEnabled = false;
        googleMap.getUiSettings().setAllGesturesEnabled(false);
    }

    private void answer(LatLng latLng) {
        tryToCancelTimer(roundTimer);
        addMarker(latLng, R.drawable.answer_icon);
        int points = GameUtils.checkAnswer(latLng, targetLocation.getLatLng());
        updateScore(points);
        endRound(points);
    }

    private class LoaderTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            fullLocationList = GameUtils.tryToGetCityList();
            Log.d("MT", "Location count: " + fullLocationList.size());
            prepareCitySet();
            return null;
        }
    }

    private void hideCountdownOverlay() {
        countdownOverlay.setVisibility(View.GONE);
    }

    private void showCountdownOverlay() {
        countdownOverlay.setVisibility(View.VISIBLE);
    }
}
