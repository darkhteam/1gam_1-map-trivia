package eu.darkh.maptrivia.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.crashlytics.android.Crashlytics;

import eu.darkh.library.utils.StorageUtils;
import eu.darkh.library.utils.UiUtils;
import eu.darkh.maptrivia.R;
import eu.darkh.maptrivia.consts.Constants;
import eu.darkh.maptrivia.fragments.OptionsFragment;
import eu.darkh.maptrivia.fragments.ScoreFragment;
import eu.darkh.maptrivia.fragments.StartFragment;
import eu.darkh.maptrivia.utils.GameUtils;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends ActionBarActivity {

    private static String SHARED_PREFERENCES_FILE = "maptriviadata";

    private OptionsFragment optionsFragment;
    private ScoreFragment scoreFragment;
    private StartFragment startFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initComponents();

        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            changeToStartFragment();
        }

    }

    private void initComponents() {
        initCrashlytics();
        StorageUtils.initStorageUtils(getBaseContext(), SHARED_PREFERENCES_FILE);
        GameUtils.initGameUtils();
    }

    private void initCrashlytics() {
        if (!Constants.IS_DEVELOPMENT) {
            Fabric.with(this, new Crashlytics());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        UiUtils.setContext(this);
        returnToPreviousFragment();
    }

    private void changeToStartFragment() {
        initStartFragment();
        startFragment(startFragment);
    }

    private void changeToOptionsFragment() {
        initOptionsFragment();
        changeFragment(optionsFragment);
    }

    private void changeToScoreFragment() {
        initScoreFragment();
        changeFragment(scoreFragment);
    }

    private void startFragment(Fragment fragment) {
        getFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, fragment)
                .commit();
    }

    private void changeFragment(Fragment fragment) {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */

    private void initOptionsFragment(){
        if (optionsFragment == null) {
            optionsFragment = new OptionsFragment();
        }
    }

    private void initScoreFragment(){
        if (scoreFragment == null) {
            scoreFragment = new ScoreFragment();
        }
    }

    private void initStartFragment(){
        if (startFragment == null) {
            startFragment = new StartFragment();
        }
    }

    public void onBackPressed() {
        if (!isBackStackEmpty()) {
            returnToPreviousFragment();
        } else {
            exitApplication();
        }
    }

    private boolean isBackStackEmpty() {
        return getFragmentManager().getBackStackEntryCount() == 0;
    }

    private void returnToPreviousFragment(){
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        }
    }

    private void exitApplication() {
        finish();
    }

    public void viewScoresAction(View view) {
        changeToScoreFragment();
    }

    public void continueGameAction(View view) {
        Intent intent = new Intent(getBaseContext(), GameActivity.class);
        startActivity(intent);
    }

    public void startNewGameAction(View view) {
        changeToOptionsFragment();
    }

}
