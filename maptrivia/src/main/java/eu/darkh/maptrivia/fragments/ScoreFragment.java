package eu.darkh.maptrivia.fragments;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import eu.darkh.library.utils.StorageUtils;
import eu.darkh.maptrivia.R;
import eu.darkh.maptrivia.adapters.ScoreAdapter;
import eu.darkh.maptrivia.enums.GameDifficulty;
import eu.darkh.maptrivia.enums.GameLength;
import eu.darkh.maptrivia.model.Score;
import eu.darkh.maptrivia.utils.GameUtils;

/**
 * Created by Aleksander_Braula on 09-Jan-15.
 */
public class ScoreFragment extends ListFragment {

    private ScoreAdapter scoreAdapter;

    private Spinner gameLengthSpinner;
    private Spinner gameDifficultySpinner;

    private GameDifficulty selectedDifficulty;
    private GameLength selectedLength;


    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        scoreAdapter = new ScoreAdapter(getActivity());
        setListAdapter(scoreAdapter);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_score, container, false);

        configureGameDifficultySpinner(view);
        configureGameLengthSpinner(view);

        return view;
    }

    private void configureGameDifficultySpinner(View view) {

        ArrayAdapter<GameDifficulty> difficultyAdapter =
                new ArrayAdapter<>(getActivity(), R.layout.spinner_item, GameDifficulty.values());

        difficultyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        gameDifficultySpinner = (Spinner) view.findViewById(R.id.score_game_difficulty_spinner);
        gameDifficultySpinner.setAdapter(difficultyAdapter);
        gameDifficultySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setDifficultyAndFilter(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    private void configureGameLengthSpinner(View view) {

        ArrayAdapter<GameLength> lengthAdapter =
                new ArrayAdapter<>(getActivity(), R.layout.spinner_item, GameLength.values());

        lengthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        gameLengthSpinner = (Spinner) view.findViewById(R.id.score_game_length_spinner);
        gameLengthSpinner.setAdapter(lengthAdapter);
        gameLengthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setLengthAndFilter(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    private void setDifficultyAndFilter(int position) {
        selectedDifficulty = GameDifficulty.values()[position];
        GameUtils.saveScoreDifficultySpinnerState(selectedDifficulty);
        filterScores();
    }

    private void setLengthAndFilter(int position) {
        selectedLength = GameLength.values()[position];
        GameUtils.saveScoreLengthSpinnerState(selectedLength);
        filterScores();
    }

    private void filterScores() {
        scoreAdapter.filterScores(selectedDifficulty, selectedLength);
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadScoreData();
        filterScores();
    }

    private void loadScoreData() {
        List<Score> scoreList = GameUtils.tryToLoadScores();
        scoreAdapter.setScoreList(scoreList);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.score_fragment_name));

        GameLength gameLength = GameUtils.loadScoreLengthSpinnerState();
        gameLengthSpinner.setSelection(gameLength.ordinal());

        GameDifficulty gameDifficulty = GameUtils.loadScoreDifficultySpinnerState();
        gameDifficultySpinner.setSelection(gameDifficulty.ordinal());
    }
}
