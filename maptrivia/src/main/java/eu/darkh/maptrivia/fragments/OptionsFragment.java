package eu.darkh.maptrivia.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import eu.darkh.library.utils.StorageUtils;
import eu.darkh.maptrivia.R;
import eu.darkh.maptrivia.activities.GameActivity;
import eu.darkh.maptrivia.enums.GameDifficulty;
import eu.darkh.maptrivia.enums.GameLength;
import eu.darkh.maptrivia.model.GameState;
import eu.darkh.maptrivia.utils.GameUtils;

/**
 * Created by Aleksander_Braula on 09-Jan-15.
 */
public class OptionsFragment extends Fragment {

    private Spinner gameLengthSpinner;
    private Spinner gameDifficultySpinner;

    private GameDifficulty selectedDifficulty;
    private GameLength selectedLength;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_options, container, false);

        configureUi(view);

        return view;
    }

    private void configureUi(View view) {
        configureGameDifficultySpinner(view);
        configureGameLengthSpinner(view);
        configureStartButton(view);
    }

    private void configureGameDifficultySpinner(View view) {

        ArrayAdapter<GameDifficulty> difficultyAdapter =
                new ArrayAdapter<>(getActivity(), R.layout.spinner_item, GameDifficulty.values());

        difficultyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        gameDifficultySpinner = (Spinner) view.findViewById(R.id.game_difficulty_spinner);
        gameDifficultySpinner.setAdapter(difficultyAdapter);

        gameDifficultySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedDifficulty = GameDifficulty.values()[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    private void configureGameLengthSpinner(View view) {

        ArrayAdapter<GameLength> lengthAdapter =
                new ArrayAdapter<>(getActivity(), R.layout.spinner_item, GameLength.values());

        lengthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        gameLengthSpinner = (Spinner) view.findViewById(R.id.game_length_spinner);
        gameLengthSpinner.setAdapter(lengthAdapter);

        gameLengthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedLength = GameLength.values()[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    private void configureStartButton(View view) {
        Button button = (Button) view.findViewById(R.id.start_game_activity_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchGameActivity();
            }
        });
    }

    private void launchGameActivity() {
        saveGameAndSpinnerState();
        Intent intent = new Intent(getActivity(), GameActivity.class);
        startActivity(intent);
    }

    private void saveGameAndSpinnerState() {
        GameState gameState = new GameState();
        gameState.setGameDifficulty(selectedDifficulty);
        gameState.setGameLength(selectedLength);
        gameState.setScore(0);
        gameState.setRound(0);

        GameUtils.saveGameState(gameState);
        GameUtils.saveOptionsDifficultySpinnerState(selectedDifficulty);
        GameUtils.saveOptionsLengthSpinnerState(selectedLength);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.options_fragment_name));

        GameLength gameLength = GameUtils.loadOptionsLengthSpinnerState();
        gameLengthSpinner.setSelection(gameLength.ordinal());

        GameDifficulty gameDifficulty = GameUtils.loadOptionsDifficultySpinnerState();
        gameDifficultySpinner.setSelection(gameDifficulty.ordinal());
    }
}
