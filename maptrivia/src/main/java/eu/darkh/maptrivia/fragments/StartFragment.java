package eu.darkh.maptrivia.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import eu.darkh.library.utils.StorageUtils;
import eu.darkh.maptrivia.R;
import eu.darkh.maptrivia.utils.GameUtils;

/**
 * Created by Aleksander_Braula on 09-Jan-15.
 */
public class StartFragment extends Fragment {

    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_start, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.app_name));
        configureContinueButton();
    }

    private void configureContinueButton() {
        boolean hasSaveGames = GameUtils.isGameFlag();

        Button continueGameButton = (Button) view.findViewById(R.id.continue_game_button);
        continueGameButton.setEnabled(hasSaveGames);
    }
}
