package eu.darkh.maptrivia.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import eu.darkh.maptrivia.R;
import eu.darkh.maptrivia.enums.GameDifficulty;
import eu.darkh.maptrivia.enums.GameLength;
import eu.darkh.maptrivia.model.Score;

/**
 * Created by Aleksander_Braula on 15-Jan-15.
 */
public class ScoreAdapter extends BaseAdapter {

    private Context context;

    private List<Score> scoreList = new ArrayList<>();
    private List<Score> baseScoreList = new ArrayList<>();

    public ScoreAdapter(Context context) {
        this.context = context;
    }

    public void setScoreList(List<Score> scoreList) {
        this.scoreList = scoreList;
        this.baseScoreList = scoreList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return scoreList.size();
    }

    @Override
    public Object getItem(int position) {
        return scoreList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            LayoutInflater li = LayoutInflater.from(context);
            view = li.inflate(R.layout.row_score_item, null);
        }

        configureRowElements(view, position);

        return view;
    }

    private void configureRowElements(View view,int position) {
        Score score = scoreList.get(position);

        TextView scoreText = (TextView) view.findViewById(R.id.row_score_text);
        scoreText.setText(score.getScore() + " points");

        TextView scoreDateText = (TextView) view.findViewById(R.id.row_score_date_text);
        scoreDateText.setText(new DateTime(score.getTimestamp()).toString("yyyy-MM-dd HH:mm:ss"));

        TextView scoreNameText = (TextView) view.findViewById(R.id.row_score_name_text);
        scoreNameText.setText(score.getName());
    }

    public void filterScores(GameDifficulty gameDifficulty, GameLength gameLength) {

        FluentIterable<Score> fluentIterable = FluentIterable.from(baseScoreList);
        fluentIterable = fluentIterable.filter(byGameDifficulty(gameDifficulty));
        fluentIterable = fluentIterable.filter(byGameLength(gameLength));

        scoreList = fluentIterable.toSortedList(new Comparator<Score>() {
            @Override
            public int compare(Score lhs, Score rhs) {
                return (int) (rhs.getTimestamp() - lhs.getTimestamp());
            }
        });

        Log.d("MT", "Filtered list: " + scoreList.size());

        notifyDataSetChanged();
    }

    private Predicate<Score> byGameLength(final GameLength gameLength) {
        return new Predicate<Score>() {
            @Override
            public boolean apply(Score input) {
                return input.getGameLength() == gameLength;
            }
        };
    }

    private Predicate<Score> byGameDifficulty(final GameDifficulty gameDifficulty) {
        return new Predicate<Score>() {
            @Override
            public boolean apply(Score input) {
                return input.getGameDifficulty() == gameDifficulty;
            }
        };
    }


}
