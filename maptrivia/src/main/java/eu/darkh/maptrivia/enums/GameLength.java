package eu.darkh.maptrivia.enums;

/**
 * Created by Aleksander_Braula on 09-Jan-15.
 */
public enum GameLength {

    SHORT("Short (10 rounds)", 10),
    MEDIUM("Medium (20 rounds)", 20),
    LONG("Long (30 rounds)", 30);

    private String text;
    private int rounds;

    GameLength(String text, int rounds) {
        this.text = text;
        this.rounds = rounds;
    }

    @Override
    public String toString() {
        return text;
    }

    public int getRounds() {
        return rounds;
    }

}
