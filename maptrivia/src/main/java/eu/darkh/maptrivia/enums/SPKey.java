package eu.darkh.maptrivia.enums;

/**
 * Created by Aleksander_Braula on 20-Jan-15.
 */
public enum SPKey {

    CITIES("cities_json"),
    GAME_STATE("game_state"),
    SCORES("scores"),
    FLAG("game_flag"),
    PLAYER_NAME("player_name"),
    SCORE_DIFFICULTY_SPINNER("score_difficulty"),
    SCORE_LENGTH_SPINNER("score_length"),
    OPTIONS_DIFFICULTY_SPINNER("options_difficulty"),
    OPTIONS_LENGTH_SPINNER("options_length");

    private String value;

    private SPKey(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
