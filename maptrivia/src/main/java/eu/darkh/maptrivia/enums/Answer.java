package eu.darkh.maptrivia.enums;

import android.graphics.Color;

import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.TreeRangeMap;

/**
 * Created by Aleksander_Braula on 13-Jan-15.
 */
public enum Answer {
    PERFECT("#5533FF33", calculateRange(0.1) , "Perfect! You get "),
    VERY_GOOD("#5566FF66", calculateRange(0.3), "Very Good!  You get "),
    GOOD("#5599FF99", calculateRange(0.5), "Good!  You get "),
    OK("#55CCFFCC", calculateRange(1.0), "Ok! You get "),
    TOO_FAR("#FFFFFFFF", 0, "Too far! You get "),
    TIMES_UP("#FFFFFFFF", 0, "Time's up! You get ");

    public static final int RANGE = 5000000;

    public static int calculateRange(double part){
        return (int)(RANGE * part);
    }

    private int circleColor;
    private int circleRadius;
    private String popupText;

    private Answer(String circleColorHash, int circleRadius, String popupText) {
        this.circleColor = Color.parseColor(circleColorHash);
        this.circleRadius = circleRadius;
        this.popupText = popupText;
    }

    public String getPopupText(){
        return popupText;
    }

    public CircleOptions getCircleOptions(LatLng center) {
        return new CircleOptions()
                .radius(circleRadius)
                .fillColor(circleColor)
                .strokeWidth(0)
                .center(center);
    }

    public static RangeMap<Integer, Answer> configureResponseMap() {
        RangeMap<Integer, Answer> responseMap = TreeRangeMap.create();
        responseMap.put(Range.atMost(0), Answer.TIMES_UP);
        responseMap.put(Range.closed(0, 0), Answer.TOO_FAR);
        responseMap.put(Range.open(0, 50), Answer.OK);
        responseMap.put(Range.closedOpen(50, 70), Answer.GOOD);
        responseMap.put(Range.closedOpen(70, 90), Answer.VERY_GOOD);
        responseMap.put(Range.atLeast(90), Answer.PERFECT);

        return responseMap;
    }

}
