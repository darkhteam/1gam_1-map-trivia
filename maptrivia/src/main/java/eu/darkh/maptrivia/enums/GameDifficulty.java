package eu.darkh.maptrivia.enums;

/**
 * Created by Aleksander_Braula on 09-Jan-15.
 */
public enum GameDifficulty {

    EASY("Easy", 10000),
    NORMAL("Normal", 7500),
    HARD("Hard", 5000);

    private String text;
    private int timeMsPerRound;

    GameDifficulty(String text, int timeMsPerRound) {
        this.text = text;
        this.timeMsPerRound = timeMsPerRound;
    }

    @Override
    public String toString() {
        return text;
    }

    public int getTimeMsPerRound() {
        return timeMsPerRound;
    }
}
